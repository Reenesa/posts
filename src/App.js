import React , {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import NavBar from './components/NavBar';
import Addpost from './components/Addpost'
import Home from './components/Home';
import Contact from  './components/Contact';
import About from './components/About';


class App extends Component {

  
  render(){
    return(
      <BrowserRouter>

      <div className='APP'>
        <NavBar/>
        <Route exact path='/' component={Home} />
        <Route path ='/about' component={About} />
        <Route path='/contact' component = {Contact} />
        <Route path='/addpost' component ={ Addpost} />
       </div> 
       </BrowserRouter>  
        );
  }  

}
export default App;

