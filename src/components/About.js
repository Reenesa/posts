import React from 'react';

const About = () => {

    return(
        <div className="container"> 
            <h2 className="center">About</h2>
            <p> This is the About page feel free to log your queries to us</p>
        </div>
    )
}

export default About;