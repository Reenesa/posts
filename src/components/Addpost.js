import React , {Component}from 'react';


class Addpost extends Component{
    state = {
        title: '',
        firstname: '',
        lastname: '',
        location: '',
        city: ''
    }

    onChangeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmitHandler =(e) =>{
        e.preventDefault();
        console.log(this.state)
        this.setState(
            {title: '',
            firstname: '',
            lastname: '',
            location: '',
            city: ''
        }
        )
    }


    render(){

        return(
            <div className="container">
                <form onSubmit={ this.onSubmitHandler} >
                    <label htmlFor='Title'> Title</label>
                    <input type='text' name='title' onChange={this.onChangeHandler}></input>
                    <label>First Name </label>
                    <input type='text' name='firstname' onChange={this.onChangeHandler}></input>
                    <label htmlFor='Last Name'> lastname</label>
                    <input type='text'  name='lastname' onChange={this.onChangeHandler}></input>
                    <label htmlFor='location'> Location</label>
                    <input type='text'  name='location' onChange={this.onChangeHandler}></input>
                    <label htmlFor='city'>City </label>
                    <input type='text' name='city' onChange={this.onChangeHandler}></input>
                    <button  type='submit' className="btn btn-primary">Submit</button>
                    
                
                </form>
            </div>
        );
    }
}

export default Addpost;