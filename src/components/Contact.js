import React from 'react';

const Contact = () => {

    return(
        <div className="container"> 
            <h2 className="center">Contact</h2>

            <p> This is the contact page feel free to log your queries to us</p>
        </div>
    )
}

export default Contact;