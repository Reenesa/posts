import React, { Component } from 'react';
import axios from 'axios';

class Home extends Component {
    state={
        books:[]
    }

    componentDidMount =()=>{
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then( resp =>{
            this.setState( {books:resp.data})
            console.log(this.state.books)
        })
    }
    render() {
        return (
            <div className="container">
                <h2 className="center">Home</h2>
                <p> This is the Home page feel free to log your queries to us</p>
                <table>
                    <tr>
                        <th>TITLE</th>
                        <th> Author</th>
                        <th> UserId</th>
                    </tr>
                    {this.state.books.map(book=>
                    <tr>
                    <td> {book.title}</td>
                    <td>{book.body}</td>
                    <td>{book.userId}</td>
                    </tr>    
                    )} 
                </table>
            </div>
        )
    }
}
export default Home;