import React from 'react';
import {Link, NavLink} from 'react-router-dom';


const NavBar = () => {

    return(
        <nav class= "nav-wrapper purple darken-3">
            <div className='container'>
                <a className='brand-logo'>Yo the best Susie!! </a>
                <ul className="right">
                    <li>< Link to='/'> Home</Link></li>
                    <li>< NavLink to='/about'>About</NavLink></li>
                    <li>< NavLink to='/contact'>Contact</NavLink></li>
                    <li>< NavLink to='/addpost'>Add Post</NavLink></li>
                </ul>             
            </div>        
        </nav>
    );
}

export default NavBar;